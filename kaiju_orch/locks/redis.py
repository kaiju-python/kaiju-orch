import textwrap
from typing import Union

from aredis.exceptions import ResponseError

from kaiju_cache.services import RedisCache

from .abc import BaseLocksService
from .etc import StatusCodes
from .exceptions import *

__all__ = ['RedisLocksService']


class RedisLocksService(BaseLocksService):
    """
    Locks service for a redis backend.

    :param app:
    :param transport: `RedisCache` class
    :param args: see `BaseLocksService`
    :param kws: see `BaseLocksService`
    :param logger:
    """

    LOCK_SCRIPT = f"""
    local e = redis.call('get', KEYS[1])
    if not e then
        redis.call('set', KEYS[1], ARGV[1])
        redis.call('expire', KEYS[1], ARGV[2])
        return redis.status_reply('{StatusCodes.OK}')
    else
        return redis.error_reply('{StatusCodes.LOCK_EXISTS}')
    end"""

    UNLOCK_SCRIPT = f"""
    local _id = redis.call('get', KEYS[1])
    if _id == ARGV[1] then
        redis.call('del', KEYS[1])
        return redis.status_reply('{StatusCodes.OK}')
    elseif not _id then
        return redis.status_reply('{StatusCodes.OK}')
    else
        return redis.error_reply('{StatusCodes.NOT_LOCK_OWNER}')
    end"""

    RENEW_SCRIPT = f"""
    for i, key in pairs(KEYS) do
        redis.call('expire', key, ARGV[i])
    end
    """

    def __init__(
            self, app, transport: Union[str, RedisCache],
            *args, logger=None, **kws
    ):
        super().__init__(app, transport, *args, **kws, logger=logger)

        # setting up all atomic lock related operations
        self._lock_script = self._transport.register_script(textwrap.dedent(self.LOCK_SCRIPT))
        self._unlock_script = self._transport.register_script(textwrap.dedent(self.UNLOCK_SCRIPT))
        self._renew_script = self._transport.register_script(textwrap.dedent(self.RENEW_SCRIPT))

    async def _check_exists(self, keys: list) -> frozenset:
        keys = await self._transport.mget(*keys, json=False)
        return frozenset(keys)

    async def _acquire(self, keys: list, identifier: str, ttl: int):
        try:
            await self._lock_script.execute(keys=keys, args=[identifier, ttl])
        except ResponseError as exc:
            if str(exc) == StatusCodes.LOCK_EXISTS:
                exc = LockExistsError(
                    'One of the provided locks exists.',
                    code=StatusCodes.LOCK_EXISTS, keys=keys)
            raise exc

    async def _release(self, keys: list, identifier: str):
        try:
            await self._unlock_script.execute(keys=keys, args=[identifier])
        except ResponseError as exc:
            if str(exc) == StatusCodes.NOT_LOCK_OWNER:
                exc = NotALockOwnerError(
                    'The lock can\'t be released not by its owner.',
                    code=StatusCodes.NOT_LOCK_OWNER, keys=keys, identifier=identifier)
            raise exc

    async def _renew(self, keys: list, values: list):
        await self._renew_script.execute(keys=keys, args=values)

    async def _owner(self, key: str):
        owner = await self._transport.get(key, json=False)
        return owner
