import abc
import asyncio
from random import random
from time import time
from typing import Optional, Union

from kaiju_tools.services import ContextableService, Service

from .etc import StatusCodes
from .exceptions import *

__all__ = ['BaseLocksService']


class BaseLocksService(ContextableService, abc.ABC):
    """
    You should inherit from this class if you want to program a custom
    backend specific lock service.

    :param app:
    :param transport: transport service (may be Redis, DB or similar)
    :param refresh_interval:  how often locks will be renewed
    :param namespace: key namespace
    :param prefix: key prefix defining that the key is a lock
    :param logger:
    """

    WAIT_RELEASE_REFRESH_INTERVAL = 0.5         #: (s) sleep interval between tries to acquire a used lock
    MIN_REFRESH_INTERVAL = 1
    REFRESH_INTERVAL = 60                       #: (s) how often locks will be renewed
    TTL_OFFSET = REFRESH_INTERVAL
    BASE_TTL = TTL_OFFSET + REFRESH_INTERVAL
    JITTER = 0.01                               #: multiplier
    NAMESPACE = '0'                             #: default namespace
    PREFIX = 'lock'                             #: lock key prefix
    DELIMITER = ':'                             #: used for delimiting sections in a key

    def __init__(
            self, app, transport: Union[str, Service],
            refresh_interval: int = REFRESH_INTERVAL,
            namespace: str = NAMESPACE, prefix: str = PREFIX,
            logger=None
    ):
        super().__init__(app=app, logger=logger)
        self._transport = self.discover_service(transport)
        self._refresh_interval = max(self.MIN_REFRESH_INTERVAL, int(refresh_interval))
        self._prefix = str(prefix)
        self._namespace = str(namespace)
        self._keys = {}
        self._closing = False
        self._daemon = None
        self._task = None
        self._task_event = None

    async def wait(self, key: str, timeout: float = None):
        """
        Wait for lock and return when it's released.

        :param key: lock key name
        :param timeout: optional max wait time in seconds
        :raises LockAcquireTimeout:
        """
        t = 0
        _key = self._create_key(key)

        while 1:
            if key in self._keys or _key in (await self._check_exists([_key])):
                self.logger.debug('Waiting for "%s" to release.', key)
                await asyncio.sleep(self.WAIT_RELEASE_REFRESH_INTERVAL)
                t += self.WAIT_RELEASE_REFRESH_INTERVAL
                if timeout and t > timeout:
                    raise LockAcquireTimeout(StatusCodes.LOCK_ACQUIRE_TIMEOUT, code=StatusCodes.LOCK_ACQUIRE_TIMEOUT)
            else:
                break

    async def acquire(self, key: str, identifier: str = None, ttl: int = None, wait=True):
        """
        Wait for lock and acquire it.

        :param key: lock name
        :param identifier: service/owner identifier, if id is None then the app['id'] will be used
        :param ttl: optional ttl in seconds, None for eternal (until app exists)
        :param wait: wait for a lock to release (if False then it will raise a `LockError`
            if lock with such key already exists
        :raises LockExistsError:
        :raises LockError:
        """

        def _wait():
            if wait:
                # self.logger.debug('Waiting for "%s" to release.', key)
                return asyncio.sleep(self.WAIT_RELEASE_REFRESH_INTERVAL)
            raise LockExistsError(StatusCodes.LOCK_EXISTS, code=StatusCodes.LOCK_EXISTS)

        if identifier is None:
            identifier = self.app['id']

        if ttl is None:
            new_ttl = self.BASE_TTL
        else:
            new_ttl = min(self.BASE_TTL, int(ttl))

        while 1:

            t = int(time()) + 1

            if self._closing:
                await _wait()
                continue

            if key in self._keys:
                _deadline = self._keys[key]
                if _deadline is None or _deadline > t:
                    await _wait()
                    continue
                else:
                    del self._keys[key]

            try:
                _key = self._create_key(key)
                await self._acquire([_key], identifier, new_ttl)
            except LockExistsError:
                await _wait()
            except Exception as exc:
                raise LockError(
                    'An unexpected error acquiring a lock',
                    code=StatusCodes.RUNTIME_ERROR, base_exc=exc) from exc
            else:
                self.logger.info('Locked "%s" ("%s") by "%s" for "%s" seconds.', key, _key, identifier, new_ttl)
                self._keys[key] = t + new_ttl if ttl else None
                break

    async def release(self, key: str, identifier: str):
        """
        Release a lock.

        :param key: lock name
        :param identifier: service/owner identifier
        :raises LockError: if the lock can't be released by this service
        """

        self.logger.info('Releasing "%s" by "%s".', key, identifier)

        try:
            _key = self._create_key(key)
            await self._release([_key], identifier)
        except NotALockOwnerError as exc:
            raise exc
        except Exception as exc:
            raise LockError(
                'An unexpected error releasing a lock',
                code=StatusCodes.RUNTIME_ERROR, base_exc=exc) from exc

        if key in self._keys:
            del self._keys[key]

    async def owner(self, key: str) -> Optional[str]:
        """Returns a current lock owner identifier or None if not found / has no owner."""

        key = self._create_key(key)
        owner = await self._owner(key)
        return owner

    @abc.abstractmethod
    async def _check_exists(self, keys: list) -> frozenset:
        """
        Check if locks with such keys exist. Return a set of existing keys.
        """

    @abc.abstractmethod
    async def _acquire(self, keys: list, identifier: str, ttl: int):
        """
        Must raise `LockExistsError` if lock exists. Also keep in mind that
        the operation must be atomic or transactional.
        """

    @abc.abstractmethod
    async def _release(self, keys: list, identifier: str):
        """
        Must raise `NotALockOwnerError` if identifier doesn't match the stored one.
        Also keep in mind that the operation must be atomic or transactional.
        """

    @abc.abstractmethod
    async def _renew(self, keys: list, values: list):
        """
        This operation should renew keys TTLs with the new provided values (in sec).
        """

    @abc.abstractmethod
    async def _owner(self, key: str):
        """Should return a key owner or None if there's no key or owner."""

    def _create_key(self, key: str):
        return self.DELIMITER.join((self._namespace, self._prefix, key))

    async def _renew_keys(self):
        """Renews existing locks."""

        await self._task_event.wait()
        self._task_event.clear()

        t = int(time()) + 1
        keys, values, to_remove = [], [], []

        for key, deadline in self._keys.items():
            if deadline is None:
                keys.append(self._create_key(key))
                values.append(self.BASE_TTL)
            elif deadline <= t:
                to_remove.append(key)
            else:
                keys.append(self._create_key(key))
                ttl = min(deadline - t, self.BASE_TTL)
                values.append(ttl)

        for key in to_remove:
            del self._keys[key]

        try:
            if keys:
                await self._renew(keys, values)
        finally:
            self._task_event.set()

    async def _loop(self):
        """The daemon which periodically starts a renew task."""

        refresh_interval = self._refresh_interval
        jitter = self.JITTER

        while not self._closing:

            t = time()

            if self._keys:
                self._task = _task = asyncio.create_task(self._renew_keys())
                try:
                    await _task
                except Exception as exc:
                    self.logger.error(
                        'An error in the daemon loop: [%s] %s',
                        exc.__class__.__name__, exc)

            t = time() - t
            dt = max(refresh_interval * (1 + random() * jitter) - t, 0)
            await asyncio.sleep(dt)

    async def init(self):
        self._closing = False
        self._task_event = asyncio.Event()
        self._task_event.set()
        self._daemon = asyncio.create_task(self._loop())
        self._keys = {}

    @property
    def closed(self) -> bool:
        return self._daemon is None

    async def close(self):
        self._closing = True
        if self._task and not self._task.done():
            await self._task_event.wait()
        self._task = None
        self._task_event = None
        self._daemon.cancel()
        self._daemon = None
